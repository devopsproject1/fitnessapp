FROM node:11.6.0-alpine AS builder
COPY . ./my-food-app
WORKDIR /my-food-app
RUN npm i
RUN $(npm bin)/ng build --prod

FROM nginx:1.15.8-alpine
COPY --from=builder /my-food-app/dist/my-food-app/ /usr/share/nginx/html
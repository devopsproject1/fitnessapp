export class User {
  id: number;
  name: string;
  gender: string;
  weight: number;
  length: number;
  age: number;
  activity: string;
  goal: string;
}
